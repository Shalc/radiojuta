package acao.comunica.com.radiojuta.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;

/**
 * @author David Emidio
 */
public class RadioService extends Service implements MediaPlayer.OnPreparedListener {

    private MediaPlayer player;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String status = intent.getStringExtra("status");
        if (status.equals("start")) {
            playMusic();
        } else {
            pauseMusic();
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        player.start();
    }

    @Override
    public void onDestroy() {
        player.release();
        super.onDestroy();
    }

    private void playMusic() {
        if (player == null) {
            configurePlayer();
        } else {
            player.start();
        }
    }

    private void pauseMusic() {
        player.pause();
    }

    private void configurePlayer() {
        try {
            player = new MediaPlayer();
            Uri uri = Uri.parse("http://172.82.131.90:8946/stream/");
            player.setDataSource(getBaseContext(), uri);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setOnPreparedListener(this);
            player.prepareAsync();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

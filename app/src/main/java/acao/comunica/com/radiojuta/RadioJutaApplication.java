package acao.comunica.com.radiojuta;

import android.app.Application;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * @author David Emidio
 */
@ReportsCrashes(
        mailTo = "projeto.comunica.acao+reportbugs@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.resToastText
)

public class RadioJutaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
}

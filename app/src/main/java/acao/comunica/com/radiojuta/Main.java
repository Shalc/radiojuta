package acao.comunica.com.radiojuta;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import acao.comunica.com.radiojuta.content.AboutFragment;
import acao.comunica.com.radiojuta.content.NewsFragment;
import acao.comunica.com.radiojuta.content.RadioFragment;
import acao.comunica.com.radiojuta.service.RadioService;


/**
 * @author David Emidio
 */
public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AudioManager audioManager;
    private int currentVolume;
    private int maxVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        configureVolume();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void configureVolume() {
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            loadFragment(new AboutFragment());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_radio) {
            loadFragment(new RadioFragment());
        } else if (id == R.id.nav_news) {
            loadFragment(new NewsFragment());
        } else if (id == R.id.nav_share) {
            shareApp();
        } else if (id == R.id.nav_fb) {
            openFB();
        } else if (id == R.id.nav_tel) {
            callTel();
        } else if (id == R.id.nav_email) {
            sendEmail();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void configureVolumeText() {
        TextView textView = (TextView) findViewById(R.id.volume_status);
        if (textView != null) {
            textView.setText("Volume: " + currentVolume);
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.content_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"central@radiojuta.org"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Contato via App");
        intent.putExtra(Intent.EXTRA_TEXT, "Olá, estou enviado este email via App...");
        try {
            startActivity(Intent.createChooser(intent, "Enviar email..."));
        } catch (Exception e) {
            Toast.makeText(this, "Não há clientes de email instalado no dispositivo", Toast.LENGTH_LONG).show();
        }
    }

    private void openFB() {
        try {
            this.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/1549538985336840")));
        } catch (PackageManager.NameNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/1549538985336840/")));
        }
    }

    private void shareApp() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Baixe o aplicativo da Rádio Web Juta!");
        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=radiojuta.comunicaacao.com.radiojuta");
        startActivity(Intent.createChooser(intent, "Compartilhar com os amigos!"));
    }

    private void callTel() {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, "Rádio Juta");

        intent.putExtra(ContactsContract.Intents.Insert.PHONE, "+5511991472450");
        intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE);

        intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE, "+551120159122");
        intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);

        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, "central@radiojuta.org");
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);

        intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_EMAIL, "projeto.comunica.acao@gmail.com");
        intent.putExtra(ContactsContract.Intents.Insert.SECONDARY_EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_OTHER);
        startActivity(intent);
    }

    public void play(View view) {
        ImageView play = (ImageView) findViewById(R.id.play);
        ImageView pause = (ImageView) findViewById(R.id.pause);
        play.setImageResource(R.drawable.ic_radio_play_active);
        pause.setImageResource(R.drawable.ic_radio_pause);
        executeService("start");
        configureRadioMessage("Rádio iniciada.");
    }

    public void pause(View view) {
        ImageView play = (ImageView) findViewById(R.id.play);
        ImageView pause = (ImageView) findViewById(R.id.pause);
        play.setImageResource(R.drawable.ic_radio_play);
        pause.setImageResource(R.drawable.ic_radio_pause_active);
        executeService("stop");
        configureRadioMessage("Rádio interrompida.");
    }

    public void decreaseVolume(View view) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume == 0 ? 0 : --currentVolume, 0);
        configureVolumeText();
    }

    public void increaseVolume(View view) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume == maxVolume ? maxVolume : ++currentVolume, 0);
        configureVolumeText();
    }

    private void executeService(String stop) {
        Intent intentService = new Intent(this, RadioService.class);
        intentService.putExtra("status", stop);
        this.startService(intentService);
    }

    public void configureRadioMessage(String message) {
        TextView radioStatus = (TextView) findViewById(R.id.radio_status);
        radioStatus.setText(message);
    }
}
